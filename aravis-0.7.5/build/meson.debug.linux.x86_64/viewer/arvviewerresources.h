#ifndef __RESOURCE_arvviewerresources_H__
#define __RESOURCE_arvviewerresources_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *arvviewerresources_get_resource (void);
#endif
