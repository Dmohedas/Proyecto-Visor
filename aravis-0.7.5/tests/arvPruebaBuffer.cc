    // system and aravis includes
    #include "glib.h"
    #include "arv.h"
    #include <stdlib.h>
    #include <signal.h>
    #include <stdio.h>
    #include <iostream>



    // opencv includes
    #include "opencv2/opencv.hpp"

    using namespace cv;
    using namespace std;


    int main (int argc, char **argv){
        ArvCamera *camera;
        ArvBuffer *buffer;
        void *framebuffer;
        framebuffer = NULL;
        IplImage *frame;
        frame = NULL;
        GError *init_error = NULL;

        camera = arv_camera_new (argc > 1 ? argv[1] : NULL, &init_error);



        /*
            Here I am setting gain to 5.85 db

         */
        arv_camera_set_gain(camera, 5.85, &init_error);

        buffer = arv_camera_acquisition (camera, 0, &init_error);

        if (ARV_IS_BUFFER (buffer)){
            printf ("Image successfully acquired\n");
            printf (" Converting Image to MAT\n");

            IplImage src;

            size_t buffer_size;
            framebuffer = (void*)arv_buffer_get_data (buffer, &buffer_size);

            cvInitImageHeader( &src, cvSize( 1280, 960 ), IPL_DEPTH_8U, 1, IPL_ORIGIN_TL, 4 );
            cvSetData(&src, framebuffer, src.widthStep);
            if ( !frame || frame->width != src.width || \
                frame->height != src.height || \
                frame->depth != src.depth || \
                frame->nChannels != src.nChannels) {
                    cvReleaseImage( &frame );
                    frame = cvCreateImage( cvGetSize(&src), src.depth, 1 );
            }
            cvCopy(&src, frame);
            cv::Mat m = cv::cvarrToMat(frame);

            imshow("frame", m);
            waitKey();
            destroyAllWindows();


        }
        else{
            printf ("Failed to acquire a single image\n");
        }

        g_clear_object (&camera);
        g_clear_object (&buffer);

        return EXIT_SUCCESS;
        return 0;
    }

