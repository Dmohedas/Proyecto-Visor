


#pragma once

#include <glib-object.h>


G_BEGIN_DECLS

/* enumerations from "arvgcconverterprivate.h" */


GType arv_gc_converter_node_type_get_type (void);
#define ARV_TYPE_GC_CONVERTER_NODE_TYPE (arv_gc_converter_node_type_get_type())

/* enumerations from "arvgvcpprivate.h" */


GType arv_gvcp_packet_type_get_type (void);
#define ARV_TYPE_GVCP_PACKET_TYPE (arv_gvcp_packet_type_get_type())


GType arv_gvcp_error_get_type (void);
#define ARV_TYPE_GVCP_ERROR (arv_gvcp_error_get_type())


GType arv_gvcp_cmd_packet_flags_get_type (void);
#define ARV_TYPE_GVCP_CMD_PACKET_FLAGS (arv_gvcp_cmd_packet_flags_get_type())


GType arv_gvcp_event_packet_flags_get_type (void);
#define ARV_TYPE_GVCP_EVENT_PACKET_FLAGS (arv_gvcp_event_packet_flags_get_type())


GType arv_gvcp_discovery_packet_flags_get_type (void);
#define ARV_TYPE_GVCP_DISCOVERY_PACKET_FLAGS (arv_gvcp_discovery_packet_flags_get_type())


GType arv_gvcp_command_get_type (void);
#define ARV_TYPE_GVCP_COMMAND (arv_gvcp_command_get_type())

/* enumerations from "arvgvspprivate.h" */


GType arv_gvsp_packet_type_get_type (void);
#define ARV_TYPE_GVSP_PACKET_TYPE (arv_gvsp_packet_type_get_type())


GType arv_gvsp_content_type_get_type (void);
#define ARV_TYPE_GVSP_CONTENT_TYPE (arv_gvsp_content_type_get_type())


GType arv_gvsp_payload_type_get_type (void);
#define ARV_TYPE_GVSP_PAYLOAD_TYPE (arv_gvsp_payload_type_get_type())

G_END_DECLS



