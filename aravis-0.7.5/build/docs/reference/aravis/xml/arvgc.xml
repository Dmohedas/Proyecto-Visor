<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
               "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd"
[
  <!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
  <!ENTITY % gtkdocentities SYSTEM "../xml/gtkdocentities.ent">
  %gtkdocentities;
]>

<refentry id="ArvGc">
<refmeta>
<refentrytitle role="top_of_page" id="ArvGc.top_of_page">ArvGc</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>ARAVIS Library</refmiscinfo>
</refmeta>
<refnamediv>
<refname>ArvGc</refname>
<refpurpose>Genicam root document class</refpurpose>
</refnamediv>

<refsect1 id="ArvGc.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="ArvGc"><returnvalue>ArvGc</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gc-new">arv_gc_new</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvGcNode"><returnvalue>ArvGcNode</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gc-get-node">arv_gc_get_node</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvDevice"><returnvalue>ArvDevice</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gc-get-device">arv_gc_get_device</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvBuffer"><returnvalue>ArvBuffer</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gc-get-buffer">arv_gc_get_buffer</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="void"><returnvalue>void</returnvalue></link>
</entry><entry role="function_name"><link linkend="arv-gc-set-buffer">arv_gc_set_buffer</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvRegisterCachePolicy"><returnvalue>ArvRegisterCachePolicy</returnvalue></link>
</entry><entry role="function_name"><link linkend="arv-gc-get-register-cache-policy">arv_gc_get_register_cache_policy</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="void"><returnvalue>void</returnvalue></link>
</entry><entry role="function_name"><link linkend="arv-gc-set-register-cache-policy">arv_gc_set_register_cache_policy</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="ArvGc.other" role="other_proto">
<title role="other_proto.title">Types and Values</title>
<informaltable role="enum_members_table" pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="name" colwidth="150px"/>
<colspec colname="description"/>
<tbody>
<row><entry role="datatype_keyword"></entry><entry role="function_name"><link linkend="ArvGc-struct">ArvGc</link></entry></row>
<row><entry role="datatype_keyword">enum</entry><entry role="function_name"><link linkend="ArvRegisterCachePolicy">ArvRegisterCachePolicy</link></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="ArvGc.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<screen>    <link linkend="GObject">GObject</link>
    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="ArvDomNode">ArvDomNode</link>
        <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="ArvDomDocument">ArvDomDocument</link>
            <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> ArvGc
</screen>
</refsect1>


<refsect1 id="ArvGc.description" role="desc">
<title role="desc.title">Description</title>
<para><link linkend="ArvGc"><type>ArvGc</type></link> implements the root document for the storage of the Genicam feature
nodes. It builds the node tree by parsing an xml file in the Genicam
standard format. See http://www.genicam.org.</para>

</refsect1>
<refsect1 id="ArvGc.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="arv-gc-new" role="function">
<title>arv_gc_new&#160;()</title>
<indexterm zone="arv-gc-new"><primary>arv_gc_new</primary></indexterm>
<programlisting language="C"><link linkend="ArvGc"><returnvalue>ArvGc</returnvalue></link>&#160;*
arv_gc_new (<parameter><link linkend="ArvDevice"><type>ArvDevice</type></link> *device</parameter>,
            <parameter>const <link linkend="void"><type>void</type></link> *xml</parameter>,
            <parameter><link linkend="size-t"><type>size_t</type></link> size</parameter>);</programlisting>
</refsect2>
<refsect2 id="arv-gc-get-node" role="function">
<title>arv_gc_get_node&#160;()</title>
<indexterm zone="arv-gc-get-node"><primary>arv_gc_get_node</primary></indexterm>
<programlisting language="C"><link linkend="ArvGcNode"><returnvalue>ArvGcNode</returnvalue></link>&#160;*
arv_gc_get_node (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>,
                 <parameter>const <link linkend="char"><type>char</type></link> *name</parameter>);</programlisting>
<para>Retrieves a genicam node by name.</para>
<refsect3 id="arv-gc-get-node.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>genicam</para></entry>
<entry role="parameter_description"><para>a <link linkend="ArvGc"><type>ArvGc</type></link> object</para></entry>
<entry role="parameter_annotations"></entry></row>
<row><entry role="parameter_name"><para>name</para></entry>
<entry role="parameter_description"><para>node name</para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gc-get-node.returns" role="returns">
<title>Returns</title>
<para> a <link linkend="ArvGcNode"><type>ArvGcNode</type></link>, null if not found. </para>
<para><emphasis role="annotation">[<acronym>transfer none</acronym>]</emphasis></para>
</refsect3></refsect2>
<refsect2 id="arv-gc-get-device" role="function">
<title>arv_gc_get_device&#160;()</title>
<indexterm zone="arv-gc-get-device"><primary>arv_gc_get_device</primary></indexterm>
<programlisting language="C"><link linkend="ArvDevice"><returnvalue>ArvDevice</returnvalue></link>&#160;*
arv_gc_get_device (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>);</programlisting>
<para>Retrieves the device handled by this genicam interface. The device is used for register access.</para>
<refsect3 id="arv-gc-get-device.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>genicam</para></entry>
<entry role="parameter_description"><para>a <link linkend="ArvGc"><type>ArvGc</type></link> object</para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gc-get-device.returns" role="returns">
<title>Returns</title>
<para> a <link linkend="ArvDevice"><type>ArvDevice</type></link>. </para>
<para><emphasis role="annotation">[<acronym>transfer none</acronym>]</emphasis></para>
</refsect3></refsect2>
<refsect2 id="arv-gc-get-buffer" role="function">
<title>arv_gc_get_buffer&#160;()</title>
<indexterm zone="arv-gc-get-buffer"><primary>arv_gc_get_buffer</primary></indexterm>
<programlisting language="C"><link linkend="ArvBuffer"><returnvalue>ArvBuffer</returnvalue></link>&#160;*
arv_gc_get_buffer (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>);</programlisting>
<para>Retrieves the binded buffer.</para>
<refsect3 id="arv-gc-get-buffer.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>genicam</para></entry>
<entry role="parameter_description"><para>a <link linkend="ArvGc"><type>ArvGc</type></link> object</para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gc-get-buffer.returns" role="returns">
<title>Returns</title>
<para> a <link linkend="ArvBuffer"><type>ArvBuffer</type></link>. </para>
<para><emphasis role="annotation">[<acronym>transfer none</acronym>]</emphasis></para>
</refsect3></refsect2>
<refsect2 id="arv-gc-set-buffer" role="function">
<title>arv_gc_set_buffer&#160;()</title>
<indexterm zone="arv-gc-set-buffer"><primary>arv_gc_set_buffer</primary></indexterm>
<programlisting language="C"><link linkend="void"><returnvalue>void</returnvalue></link>
arv_gc_set_buffer (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>,
                   <parameter><link linkend="ArvBuffer"><type>ArvBuffer</type></link> *buffer</parameter>);</programlisting>
</refsect2>
<refsect2 id="arv-gc-get-register-cache-policy" role="function">
<title>arv_gc_get_register_cache_policy&#160;()</title>
<indexterm zone="arv-gc-get-register-cache-policy"><primary>arv_gc_get_register_cache_policy</primary></indexterm>
<programlisting language="C"><link linkend="ArvRegisterCachePolicy"><returnvalue>ArvRegisterCachePolicy</returnvalue></link>
arv_gc_get_register_cache_policy (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>);</programlisting>
</refsect2>
<refsect2 id="arv-gc-set-register-cache-policy" role="function">
<title>arv_gc_set_register_cache_policy&#160;()</title>
<indexterm zone="arv-gc-set-register-cache-policy"><primary>arv_gc_set_register_cache_policy</primary></indexterm>
<programlisting language="C"><link linkend="void"><returnvalue>void</returnvalue></link>
arv_gc_set_register_cache_policy (<parameter><link linkend="ArvGc"><type>ArvGc</type></link> *genicam</parameter>,
                                  <parameter><link linkend="ArvRegisterCachePolicy"><type>ArvRegisterCachePolicy</type></link> policy</parameter>);</programlisting>
</refsect2>

</refsect1>
<refsect1 id="ArvGc.other_details" role="details">
<title role="details.title">Types and Values</title>
<refsect2 id="ArvGc-struct" role="struct">
<title>ArvGc</title>
<indexterm zone="ArvGc-struct"><primary>ArvGc</primary></indexterm>
<programlisting language="C">typedef struct _ArvGc ArvGc;</programlisting>
</refsect2>
<refsect2 id="ArvRegisterCachePolicy" role="enum" condition="since:0.8.0">
<title>enum ArvRegisterCachePolicy</title>
<indexterm zone="ArvRegisterCachePolicy" role="0.8.0"><primary>ArvRegisterCachePolicy</primary></indexterm>
<refsect3 id="ArvRegisterCachePolicy.members" role="enum_members">
<title>Members</title>
<informaltable role="enum_members_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="enum_members_name" colwidth="300px"/>
<colspec colname="enum_members_description"/>
<colspec colname="enum_members_annotations" colwidth="200px"/>
<tbody>
<row role="constant"><entry role="enum_member_name"><para id="ARV-REGISTER-CACHE-POLICY-DISABLE:CAPS">ARV_REGISTER_CACHE_POLICY_DISABLE</para></entry>
<entry role="enum_member_description"><para>disable register caching</para>
</entry>
<entry role="enum_member_annotations"></entry>
</row>
<row role="constant"><entry role="enum_member_name"><para id="ARV-REGISTER-CACHE-POLICY-ENABLE:CAPS">ARV_REGISTER_CACHE_POLICY_ENABLE</para></entry>
<entry role="enum_member_description"><para>enable register caching</para>
</entry>
<entry role="enum_member_annotations"></entry>
</row>
<row role="constant"><entry role="enum_member_name"><para id="ARV-REGISTER-CACHE-POLICY-DEBUG:CAPS">ARV_REGISTER_CACHE_POLICY_DEBUG</para></entry>
<entry role="enum_member_description"><para>enable register caching, but read the acual register value for comparison</para>
</entry>
<entry role="enum_member_annotations"></entry>
</row>
<row role="constant"><entry role="enum_member_name"><para id="ARV-REGISTER-CACHE-POLICY-DEFAULT:CAPS">ARV_REGISTER_CACHE_POLICY_DEFAULT</para></entry>
<entry role="enum_member_description"><para>default cache policy</para>
</entry>
<entry role="enum_member_annotations"></entry>
</row>
</tbody></tgroup></informaltable>
</refsect3><para role="since">Since: <link linkend="api-index-0.8.0">0.8.0</link></para></refsect2>

</refsect1>

</refentry>
