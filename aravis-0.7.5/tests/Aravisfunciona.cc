/*#include "glib.h"
  	#include "arv.h"
  #include <stdlib.h>
    #include <signal.h>
    #include <stdio.h>
    #include <iostream>


    // opencv includes
    #include "opencv2/opencv.hpp"

    using namespace cv;
    using namespace std;

    int main(){

        std::cout << "OpenCV Version : " << CV_VERSION << std::endl ;
        printf("Value of CAP_ARAVIS is %d\n", cv::CAP_ARAVIS);
        cv::VideoCapture cam; // constant for ARAVIS SDK
        cam.open(2100);

        std::cout << "Camera Status : " << cam.isOpened() << std::endl;
        if(!cam.isOpened()){
            printf("Error in opening camera\n");
            return 0;
        }

        cv::namedWindow("frame", 1);
        while(true){
            cv::Mat frame;
            bool ret = cam.read(frame);
            cv::imshow("frame", frame);
            int ch = cv::waitKey(1) & 0xFF;
            if (ch == 27){
                break;
            }

        }
        cam.release();
        cv::destroyAllWindows();

        return 0;

    }
*/

/*#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"


#include <iostream>

using namespace std;
using namespace cv;

int main() {

    Mat imagen = imread("C:\\OpenCV\\lena_std.tif", CV_LOAD_IMAGE_GRAYSCALE); //Guarda la imagen en la matriz en grises
    imshow("Original", imagen);
    cout << "La imagen tiene " << imagen.rows << " pixeles de alto y " << imagen.cols << " pixeles de ancho" << endl;
    uchar pixel = imagen.at<uchar>(511, 511);//Con la func "at" se pide qué hay en ese pixel, y se pide el formato
    cout << "En la coord. 511 x 511 el nivel de gris es " <<(int)pixel<<endl;
    waitKey(0);
    return 1;
}
*/

/*
 * main.cpp
 *
 *  Created on: 9 jul. 2020
 *      Author: dmohedas
 */
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file
    /*image = imread("/home/dmohedas/Escritorio/pingu.jpg", CV_LOAD_IMAGE_COLOR);*/

    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", image );                   // Show our image inside it.

   /*imwrite( "/home/dmohedas/Imágenes/Aravis/Gray_Image.jpg", image );*/
    waitKey(0);                                          // Wait for a keystroke in the window
    return 0;
}
