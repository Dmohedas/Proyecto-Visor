<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
               "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd"
[
  <!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
  <!ENTITY % gtkdocentities SYSTEM "../xml/gtkdocentities.ent">
  %gtkdocentities;
]>

<refentry id="ArvGvFakeCamera">
<refmeta>
<refentrytitle role="top_of_page" id="ArvGvFakeCamera.top_of_page">ArvGvFakeCamera</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>ARAVIS Library</refmiscinfo>
</refmeta>
<refnamediv>
<refname>ArvGvFakeCamera</refname>
<refpurpose>GigE Vision Simulator</refpurpose>
</refnamediv>

<refsect1 id="ArvGvFakeCamera.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="ArvGvFakeCamera"><returnvalue>ArvGvFakeCamera</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gv-fake-camera-new">arv_gv_fake_camera_new</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvGvFakeCamera"><returnvalue>ArvGvFakeCamera</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gv-fake-camera-new-full">arv_gv_fake_camera_new_full</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="ArvFakeCamera"><returnvalue>ArvFakeCamera</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="arv-gv-fake-camera-get-fake-camera">arv_gv_fake_camera_get_fake_camera</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="gboolean"><returnvalue>gboolean</returnvalue></link>
</entry><entry role="function_name"><link linkend="arv-gv-fake-camera-is-running">arv_gv_fake_camera_is_running</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="ArvGvFakeCamera.properties" role="properties">
<title role="properties.title">Properties</title>
<informaltable frame="none">
<tgroup cols="3">
<colspec colname="properties_type" colwidth="150px"/>
<colspec colname="properties_name" colwidth="300px"/>
<colspec colname="properties_flags" colwidth="200px"/>
<tbody>
<row><entry role="property_type"><link linkend="gchar"><type>gchar</type></link>&#160;*</entry><entry role="property_name"><link linkend="ArvGvFakeCamera--genicam-filename">genicam-filename</link></entry><entry role="property_flags">Write / Construct Only</entry></row>
<row><entry role="property_type"><link linkend="gdouble"><type>gdouble</type></link></entry><entry role="property_name"><link linkend="ArvGvFakeCamera--gvsp-lost-ratio">gvsp-lost-ratio</link></entry><entry role="property_flags">Write / Construct</entry></row>
<row><entry role="property_type"><link linkend="gchar"><type>gchar</type></link>&#160;*</entry><entry role="property_name"><link linkend="ArvGvFakeCamera--interface-name">interface-name</link></entry><entry role="property_flags">Write / Construct Only</entry></row>
<row><entry role="property_type"><link linkend="gchar"><type>gchar</type></link>&#160;*</entry><entry role="property_name"><link linkend="ArvGvFakeCamera--serial-number">serial-number</link></entry><entry role="property_flags">Write / Construct Only</entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="ArvGvFakeCamera.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<screen>    <link linkend="GObject">GObject</link>
    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> ArvGvFakeCamera
</screen>
</refsect1>


<refsect1 id="ArvGvFakeCamera.description" role="desc">
<title role="desc.title">Description</title>
<para><link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link> is a class that simulates a real GigEVision camera.</para>

</refsect1>
<refsect1 id="ArvGvFakeCamera.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="arv-gv-fake-camera-new" role="function" condition="since:0.8.0">
<title>arv_gv_fake_camera_new&#160;()</title>
<indexterm zone="arv-gv-fake-camera-new" role="0.8.0"><primary>arv_gv_fake_camera_new</primary></indexterm>
<programlisting language="C"><link linkend="ArvGvFakeCamera"><returnvalue>ArvGvFakeCamera</returnvalue></link>&#160;*
arv_gv_fake_camera_new (<parameter>const <link linkend="char"><type>char</type></link> *interface_name</parameter>,
                        <parameter>const <link linkend="char"><type>char</type></link> *serial_number</parameter>);</programlisting>
<refsect3 id="arv-gv-fake-camera-new.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>interface_name</para></entry>
<entry role="parameter_description"><para> listening network interface ('lo' by default). </para></entry>
<entry role="parameter_annotations"><emphasis role="annotation">[<acronym>nullable</acronym>]</emphasis></entry></row>
<row><entry role="parameter_name"><para>serial_number</para></entry>
<entry role="parameter_description"><para> fake device serial number ('GV01' by default). </para></entry>
<entry role="parameter_annotations"><emphasis role="annotation">[<acronym>nullable</acronym>]</emphasis></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gv-fake-camera-new.returns" role="returns">
<title>Returns</title>
<para> a new <link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link></para>
</refsect3><para role="since">Since: <link linkend="api-index-0.8.0">0.8.0</link></para></refsect2>
<refsect2 id="arv-gv-fake-camera-new-full" role="function" condition="since:0.8.0">
<title>arv_gv_fake_camera_new_full&#160;()</title>
<indexterm zone="arv-gv-fake-camera-new-full" role="0.8.0"><primary>arv_gv_fake_camera_new_full</primary></indexterm>
<programlisting language="C"><link linkend="ArvGvFakeCamera"><returnvalue>ArvGvFakeCamera</returnvalue></link>&#160;*
arv_gv_fake_camera_new_full (<parameter>const <link linkend="char"><type>char</type></link> *interface_name</parameter>,
                             <parameter>const <link linkend="char"><type>char</type></link> *serial_number</parameter>,
                             <parameter>const <link linkend="char"><type>char</type></link> *genicam_filename</parameter>);</programlisting>
<refsect3 id="arv-gv-fake-camera-new-full.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>interface_name</para></entry>
<entry role="parameter_description"><para> listening network interface, default is lo. </para></entry>
<entry role="parameter_annotations"><emphasis role="annotation">[<acronym>nullable</acronym>]</emphasis></entry></row>
<row><entry role="parameter_name"><para>serial_number</para></entry>
<entry role="parameter_description"><para> fake device serial number, default is GV01. </para></entry>
<entry role="parameter_annotations"><emphasis role="annotation">[<acronym>nullable</acronym>]</emphasis></entry></row>
<row><entry role="parameter_name"><para>genicam_filename</para></entry>
<entry role="parameter_description"><para> path to alternative genicam data. </para></entry>
<entry role="parameter_annotations"><emphasis role="annotation">[<acronym>nullable</acronym>]</emphasis></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gv-fake-camera-new-full.returns" role="returns">
<title>Returns</title>
<para> a new <link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link></para>
</refsect3><para role="since">Since: <link linkend="api-index-0.8.0">0.8.0</link></para></refsect2>
<refsect2 id="arv-gv-fake-camera-get-fake-camera" role="function" condition="since:0.8.0">
<title>arv_gv_fake_camera_get_fake_camera&#160;()</title>
<indexterm zone="arv-gv-fake-camera-get-fake-camera" role="0.8.0"><primary>arv_gv_fake_camera_get_fake_camera</primary></indexterm>
<programlisting language="C"><link linkend="ArvFakeCamera"><returnvalue>ArvFakeCamera</returnvalue></link>&#160;*
arv_gv_fake_camera_get_fake_camera (<parameter><link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link> *gv_fake_camera</parameter>);</programlisting>
<para>Retrieves the underlying <link linkend="ArvFakeCamera"><type>ArvFakeCamera</type></link> object owned by <parameter>gv_fake_camera</parameter>
.</para>
<refsect3 id="arv-gv-fake-camera-get-fake-camera.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>gv_fake_camera</para></entry>
<entry role="parameter_description"><para>a <link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gv-fake-camera-get-fake-camera.returns" role="returns">
<title>Returns</title>
<para> underlying fake camera object. </para>
<para><emphasis role="annotation">[<acronym>transfer none</acronym>]</emphasis></para>
</refsect3><para role="since">Since: <link linkend="api-index-0.8.0">0.8.0</link></para></refsect2>
<refsect2 id="arv-gv-fake-camera-is-running" role="function" condition="since:0.8.0">
<title>arv_gv_fake_camera_is_running&#160;()</title>
<indexterm zone="arv-gv-fake-camera-is-running" role="0.8.0"><primary>arv_gv_fake_camera_is_running</primary></indexterm>
<programlisting language="C"><link linkend="gboolean"><returnvalue>gboolean</returnvalue></link>
arv_gv_fake_camera_is_running (<parameter><link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link> *gv_fake_camera</parameter>);</programlisting>
<refsect3 id="arv-gv-fake-camera-is-running.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>gv_fake_camera</para></entry>
<entry role="parameter_description"><para>a <link linkend="ArvGvFakeCamera"><type>ArvGvFakeCamera</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="arv-gv-fake-camera-is-running.returns" role="returns">
<title>Returns</title>
<para> <link linkend="TRUE:CAPS"><literal>TRUE</literal></link> if the fake camera is correctly listening on the GVCP port</para>
</refsect3><para role="since">Since: <link linkend="api-index-0.8.0">0.8.0</link></para></refsect2>

</refsect1>
<refsect1 id="ArvGvFakeCamera.other_details" role="details">
<title role="details.title">Types and Values</title>

</refsect1>
<refsect1 id="ArvGvFakeCamera.property-details" role="property_details">
<title role="property_details.title">Property Details</title>
<refsect2 id="ArvGvFakeCamera--genicam-filename" role="property"><title>The <literal>“genicam-filename”</literal> property</title>
<indexterm zone="ArvGvFakeCamera--genicam-filename"><primary>ArvGvFakeCamera:genicam-filename</primary></indexterm>
<programlisting>  “genicam-filename”         <link linkend="gchar"><type>gchar</type></link>&#160;*</programlisting>
<para>Genicam filename.</para><para>Flags: Write / Construct Only</para>
<para>Default value: NULL</para>
</refsect2>
<refsect2 id="ArvGvFakeCamera--gvsp-lost-ratio" role="property"><title>The <literal>“gvsp-lost-ratio”</literal> property</title>
<indexterm zone="ArvGvFakeCamera--gvsp-lost-ratio"><primary>ArvGvFakeCamera:gvsp-lost-ratio</primary></indexterm>
<programlisting>  “gvsp-lost-ratio”          <link linkend="gdouble"><type>gdouble</type></link></programlisting>
<para>GVSP lost packet ratio.</para><para>Flags: Write / Construct</para>
<para>Allowed values: [0,1]</para>
<para>Default value: 0</para>
</refsect2>
<refsect2 id="ArvGvFakeCamera--interface-name" role="property"><title>The <literal>“interface-name”</literal> property</title>
<indexterm zone="ArvGvFakeCamera--interface-name"><primary>ArvGvFakeCamera:interface-name</primary></indexterm>
<programlisting>  “interface-name”           <link linkend="gchar"><type>gchar</type></link>&#160;*</programlisting>
<para>Interface name.</para><para>Flags: Write / Construct Only</para>
<para>Default value: "lo"</para>
</refsect2>
<refsect2 id="ArvGvFakeCamera--serial-number" role="property"><title>The <literal>“serial-number”</literal> property</title>
<indexterm zone="ArvGvFakeCamera--serial-number"><primary>ArvGvFakeCamera:serial-number</primary></indexterm>
<programlisting>  “serial-number”            <link linkend="gchar"><type>gchar</type></link>&#160;*</programlisting>
<para>Serial number.</para><para>Flags: Write / Construct Only</para>
<para>Default value: "GV01"</para>
</refsect2>

</refsect1>

</refentry>
