/* Aravis - Digital camera library
 *
 * Copyright © 2009-2019 Emmanuel Pacaud
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: Emmanuel Pacaud <emmanuel@gnome.org>
 */

#include <arv.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <png.h>




#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>

using namespace cv;
using namespace std;

Mat src, dst, tmp;
char * window_name = "Zoom demo";

static char *arv_option_device_name = NULL;
static char *arv_option_device_address = NULL;
static char *arv_option_debug_domains = NULL;
static char *arv_option_cache_policy = NULL;
static gboolean arv_option_show_time = FALSE;

static const GOptionEntry arv_option_entries[] =
{
	{ "nombre",		'n', 0, G_OPTION_ARG_STRING,
		&arv_option_device_name,	NULL, "<nombre del dispositivo>"},
	{ "direccion",		'a', 0, G_OPTION_ARG_STRING,
		&arv_option_device_address,	NULL, "<direccion del dispositivo>"},
	{ "cache",	'c', 0, G_OPTION_ARG_STRING,
		&arv_option_cache_policy, 	"Registro cache", "[deshabilitado|habilitado|debug]" },
	{ "tiempo",		't', 0, G_OPTION_ARG_NONE,
		&arv_option_show_time, 		"Mostrar tiempo de ejecucion", NULL},
	{ "debug", 		'd', 0, G_OPTION_ARG_STRING,
		&arv_option_debug_domains, 	NULL, "<categoria>[:<nivel>][,...]" },
	{ NULL }
};

static const char
description_content[] =
		"Hay que elegir un comando entre las siguientes posibilidades:\n"
		"\n"
		"genicam:                           volcar el contenido de los datos xml de Genicam\n"
		"parametros:                        enumerar todos los parametros\n"
		"valores:                           enumerar todos los valores de las caracteristicas disponibles\n"
		"descripcion [<parametro>] ...:     mostrar la descripción completa del parametro elegido\n"
		"control <parametro>[=<valor>] ...: leer/escribir en el registro correspondiente al parametro\n"
		"captura [<archivo png>]:           guarda una captura en png\n"
		"monocolor [<archivo png>]:         transforma imagen en color en escala de grises\n"
		"zoom [<imagen>]:		   aumentar o disminuir zoom de una imagen\n"
		"brillo [<imagen>]:		   cambiar el brillo y contraste de una imagen\n"
		"videotest [<archivo mp4>]:         apertura de un archivo.mp4\n"
		"\n"
		"Si no se utiliza ningun comando, se enumeraran todos los dispositivos disponibles.\n"
		"Para el comando de control, el acceso directo a los registros del dispositivo se proporciona mediante una sintaxis R [direccion] "
		"en lugar de un nombre de caracteristica.\n"
		"\n"
		"Ejemplos:\n"
		"\n"
		"arv-toolOPEN-0.8  control Width=128 Height=128 Gain R[0x10000]=0x10\n"
		"arv-toolOPEN-0.8  parametros\n"
		"arv-toolOPEN-0.8  descripcion Width Height\n"
		"arv-toolOPEN-0.8  captura\n"
		"arv-toolOPEN-0.8  monocolor\n"
		"arv-toolOPEN-0.8  zoom\n"
		"arv-toolOPEN-0.8  brillo\n"
		"arv-toolOPEN-0.8  videotest\n"
		"arv-toolOPEN-0.8 -n Basler-210ab4 genicam\n";


typedef enum {
	ARV_TOOL_LIST_MODE_FEATURES,
	ARV_TOOL_LIST_MODE_DESCRIPTIONS,
	ARV_TOOL_LIST_MODE_VALUES
} ArvToolListMode;


int videostest(){

  // Create a VideoCapture object and open the input file
  // If the input is the web camera, pass 0 instead of the video file name
  VideoCapture cap("/home/dmohedas/Escritorio/Conexion2PCs.mp4");
  /*VideoCapture cap(0);*/

  // Check if camera opened successfully
  if(!cap.isOpened()){
    cout << "Error en la apertura de archivo de video" << endl;
    return -1;
  }

  while(1){

    Mat frame;
    // Capture frame-by-frame
    cap >> frame;

    // If the frame is empty, break immediately
    if (frame.empty())
      break;

    // Display the resulting frame
    imshow( "Video", frame );

    // Press  ESC on keyboard to exit
    char c=(char)waitKey(25);
    if(c==27)
      break;
  }

  // When everything done, release the video capture object
  cap.release();

  // Closes all the frames
  destroyAllWindows();

  return EXIT_SUCCESS;
  return 0;
}

int brillo( int argc, char ** argv )
{

    /*Mat imgBrillo = imread("/home/dmohedas/Escritorio/pingu.jpg", CV_LOAD_IMAGE_COLOR);*/
    Mat imgBrillo = imread("/home/dmohedas/Escritorio/lena.png", CV_LOAD_IMAGE_COLOR);

    if (imgBrillo.empty())
    {
        cout << "Imagen no puede ser cargada!" << endl;
        return -1;
    }

    Mat img_higher_contrast;
    imgBrillo.convertTo(img_higher_contrast, -1, 2, 0); //increase the contrast (double)

    Mat img_lower_contrast;
    imgBrillo.convertTo(img_lower_contrast, -1, 0.5, 0); //decrease the contrast (halve)

    Mat img_higher_brightness;
    imgBrillo.convertTo(img_higher_brightness, -1, 1, 20); //increase the brightness by 20 for each pixel

    Mat img_lower_brightness;
    imgBrillo.convertTo(img_lower_brightness, -1, 1, -20); //decrease the brightness by 20 for each pixel

    //create windows
    namedWindow("Imagen Original", CV_WINDOW_AUTOSIZE);
    namedWindow("Contraste Alto", CV_WINDOW_AUTOSIZE);
    namedWindow("Contraste Bajo", CV_WINDOW_AUTOSIZE);
    namedWindow("Brillo Alto", CV_WINDOW_AUTOSIZE);
    namedWindow("Brillo Bajo", CV_WINDOW_AUTOSIZE);
    //show the image
    imshow("Imagen Original", imgBrillo);
    imshow("Contraste Alto", img_higher_contrast);
    imshow("Contraste Bajo", img_lower_contrast);
    imshow("Brillo Alto", img_higher_brightness);
    imshow("Brillo Bajo", img_lower_brightness);

    waitKey(0); //wait for key press
    /*     destroyAllWindows(); destroy all open windows*/
    return EXIT_SUCCESS;
    return 0;
}


int zoomimagen( int argc, char ** argv )
{
/// Instrucciones
printf( "\n Zoom In-Out demo \n " );
printf( "------------------ \n" );
printf( " * [u] -> Zoom x2 \n" );
printf( " * [d] -> Zoom /2 \n" );
printf( " * [ESC] -> Cerrar programa \n \n" );

/// Test image - Make sure it s divisible by 2^{n}

src = imread("/home/dmohedas/Escritorio/pingu.jpg", CV_LOAD_IMAGE_COLOR);
if( !src.data )
{
	printf(" No existe imagen -- Se va a cerrar el programa\n");
	return -1;
}

tmp = src;
dst = tmp;

/// Crear Ventana
namedWindow( window_name, CV_WINDOW_AUTOSIZE );
imshow( window_name, dst );

/// Loop
	while( true )
	{
		int c;
		c = waitKey(10);


		if((char)c == 27 ){

		break;
		}

		if ((char)c == 'u' ){

		pyrUp( tmp, dst, Size( tmp.cols*2, tmp.rows*2 ) );
		printf( "** Zoom In: Imagen x 2 \n" );
		}

		else if( (char)c == 'd' )
		{
			pyrDown( tmp, dst, Size( tmp.cols/2, tmp.rows/2 ) );
		printf( "** Zoom Out: Imagen / 2 \n" );
		}
		imshow( window_name, dst );
		tmp = dst;
		}

		return EXIT_SUCCESS;
		return 0;
		}



int monocolorcambio( int argc, char** argv )
{

	/*char* imageName = argv[1];*/

 Mat imageMonocolor;
 imageMonocolor = imread("/home/dmohedas/Escritorio/lena.png", CV_LOAD_IMAGE_COLOR);

 if( argc != 2 || !imageMonocolor.data )
 {
   printf( " No existe imagen \n " );
   return -1;
 }

 Mat gray_image;
 cvtColor( imageMonocolor, gray_image, CV_BGR2GRAY );


 imwrite( "/home/dmohedas/Imágenes/Aravis/LenaENGRIS.jpg", gray_image );

 namedWindow( "Mono", CV_WINDOW_AUTOSIZE );
 namedWindow( "Mono_gris", CV_WINDOW_AUTOSIZE );

 imshow( "Mono", imageMonocolor );
 imshow( "Mono_gris", gray_image );

 waitKey(0);

 return EXIT_SUCCESS;
 return 0;
}

static void
captura(int argc, char **argv)
{

			ArvCamera *camera;
	        ArvBuffer *buffer;
	        void *framebuffer;
	        framebuffer = NULL;
	        IplImage *frame;
	        frame = NULL;
	        GError *init_error = NULL;

	       /* void *data = g_malloc (1024); Pruebas buffer*/


	        camera = arv_camera_new (argc > 1 ? argv[1] : NULL, &init_error);


	        /*buffer = arv_buffer_new (1024, data);*/
	        /*buffer = arv_buffer_new_allocate(1228800); /*(1280 x 960)*/
	        buffer = arv_camera_acquisition (camera, 0, &init_error);
	       /* buffer = arv_buffer_new_allocate(250000);*/

	        if (ARV_IS_BUFFER (buffer)){
	            printf ("Captura de imagen realizada\n");

	            IplImage src;

	            size_t buffer_size;
	            framebuffer = (void*)arv_buffer_get_data (buffer, &buffer_size);

	            cvInitImageHeader( &src, cvSize( 50, 50 ), IPL_DEPTH_8U, 1, IPL_ORIGIN_TL, 4 );
	            cvSetData(&src, framebuffer, src.widthStep);
	            if ( !frame || frame->width != src.width || \
	                frame->height != src.height || \
	                frame->depth != src.depth || \
	                frame->nChannels != src.nChannels) {
	                    cvReleaseImage( &frame );
	                    frame = cvCreateImage( cvGetSize(&src), src.depth, 1 );
	            }
	            cvCopy(&src, frame);
	            cv::Mat m = cv::cvarrToMat(frame);

	            imshow("Imagen capturada", m);
	            imwrite( "/home/dmohedas/Imágenes/Aravis/Captura.jpg", m );
	            waitKey();
	            destroyAllWindows();


	        }
	        else{
	            printf ("Failed to acquire a single image\n");
	        }

	        g_clear_object (&camera);
	        g_clear_object (&buffer);



	        return EXIT_SUCCESS;
	        return 0;

}





static void
arv_tool_list_features (ArvGc *genicam, const char *feature, ArvToolListMode list_mode, int level)
{
	ArvGcNode *node;

	node = arv_gc_get_node (genicam, feature);
	if (ARV_IS_GC_FEATURE_NODE (node) &&
	    arv_gc_feature_node_is_implemented (ARV_GC_FEATURE_NODE (node), NULL)) {

		if (ARV_IS_GC_CATEGORY (node)) {
			printf ("%*s%-12s: '%s'\n", 4 * level, "",
				arv_dom_node_get_node_name (ARV_DOM_NODE (node)),
				feature);
		} else {
			if (arv_gc_feature_node_is_available (ARV_GC_FEATURE_NODE (node), NULL)) {
				char *value = NULL;
				GError *error = NULL;
				gboolean is_selector;

				if (list_mode == ARV_TOOL_LIST_MODE_VALUES) {
					const char *unit;

					if (ARV_IS_GC_STRING (node) ||
					    ARV_IS_GC_ENUMERATION (node)) {
						value = g_strdup_printf ("'%s'", arv_gc_string_get_value (ARV_GC_STRING (node), &error));
					} else if (ARV_IS_GC_INTEGER (node)) {
						if (ARV_IS_GC_ENUMERATION (node)) {
							value = g_strdup_printf ("'%s'",
										 arv_gc_string_get_value (ARV_GC_STRING (node), &error));
						} else {
							unit = arv_gc_integer_get_unit (ARV_GC_INTEGER (node), NULL);

							value = g_strdup_printf ("%" G_GINT64_FORMAT "%s%s",
										 arv_gc_integer_get_value (ARV_GC_INTEGER (node), &error),
										 unit != NULL ? " " : "",
										 unit != NULL ? unit : "");
						}
					} else if (ARV_IS_GC_FLOAT (node)) {
						unit = arv_gc_float_get_unit (ARV_GC_FLOAT (node), NULL);

						value = g_strdup_printf ("%g%s%s",
									 arv_gc_float_get_value (ARV_GC_FLOAT (node), &error),
									 unit != NULL ? " " : "",
									 unit != NULL ? unit : "");
					} else if (ARV_IS_GC_BOOLEAN (node)) {
						value = g_strdup_printf ("%s",
									 arv_gc_boolean_get_value (ARV_GC_BOOLEAN (node), &error) ?
									 "true" : "false");
					}
				}

				is_selector = ARV_IS_GC_SELECTOR (node) && arv_gc_selector_is_selector (ARV_GC_SELECTOR (node));

				if (error != NULL) {
					g_clear_error (&error);
				} else {
					if (value != NULL && value[0] != '\0')
						printf ("%*s%-12s: '%s' = %s\n", 4 * level, "",
							arv_dom_node_get_node_name (ARV_DOM_NODE (node)), feature, value);
					else
						printf ("%*s%-12s: '%s'\n", 4 * level, "",
							arv_dom_node_get_node_name (ARV_DOM_NODE (node)), feature);

					if (is_selector) {
						const GSList *iter;

						for (iter = arv_gc_selector_get_selected_features (ARV_GC_SELECTOR (node));
						     iter != NULL;
						     iter = iter->next) {
							printf (" %*s     * %s\n", 4 * level, " ",
								arv_gc_feature_node_get_name (iter->data));
						}

					}
				}

				g_clear_pointer (&value, g_free);
			} else {
				if (list_mode == ARV_TOOL_LIST_MODE_FEATURES)
					printf ("%*s%-12s: '%s' (No disponible)\n", 4 * level, "",
						arv_dom_node_get_node_name (ARV_DOM_NODE (node)), feature);
			}
		}

		if (list_mode == ARV_TOOL_LIST_MODE_DESCRIPTIONS) {
			const char *description;

			description = arv_gc_feature_node_get_description (ARV_GC_FEATURE_NODE (node));
			if (description)
				printf ("%s\n", description);
		}

		if (ARV_IS_GC_CATEGORY (node)) {
			const GSList *features;
			const GSList *iter;

			features = arv_gc_category_get_features (ARV_GC_CATEGORY (node));

			for (iter = features; iter != NULL; iter = iter->next)
				arv_tool_list_features (genicam, iter->data, list_mode, level + 1);
		} else if (ARV_IS_GC_ENUMERATION (node) && list_mode == ARV_TOOL_LIST_MODE_FEATURES) {
			const GSList *childs;
			const GSList *iter;

			childs = arv_gc_enumeration_get_entries (ARV_GC_ENUMERATION (node));
			for (iter = childs; iter != NULL; iter = iter->next) {
				if (arv_gc_feature_node_is_implemented (iter->data, NULL)) {
					printf ("%*s%-12s: '%s'%s\n", 4 * (level + 1), "",
						arv_dom_node_get_node_name (iter->data),
						arv_gc_feature_node_get_name (iter->data),
						arv_gc_feature_node_is_available (iter->data, NULL) ? "" : " (No disponible)");
				}
			}
		}
	}
}

static void
arv_tool_execute_command (int argc, char **argv, ArvDevice *device, ArvRegisterCachePolicy cache_policy)
{
	ArvGc *genicam;
	const char *command = argv[1];
	gint64 start;

	if (device == NULL || argc < 2)
		return;

	arv_device_set_register_cache_policy (device, cache_policy);

	genicam = arv_device_get_genicam (device);

	start = g_get_monotonic_time ();

	if (g_strcmp0 (command, "genicam") == 0) {
		const char *xml;
		size_t size;

		xml = arv_device_get_genicam_xml (device, &size);
		if (xml != NULL)
			printf ("%*s\n", (int) size, xml);
	} else if (g_strcmp0 (command, "parametros") == 0) {
		arv_tool_list_features (genicam, "Root", ARV_TOOL_LIST_MODE_FEATURES, 0);
	}
	else if (argc > 1 && g_strcmp0 (argv[1], "captura") == 0) {

			/*arv_device_set_string_feature_value (ArvDevice *device, const char *feature, const char *value, GError **error);*/
			/*arv_device_set_string_feature_value ((*device), "GevGVSPExtendedIDMode", "Off", NULL);*/

			captura(argc, argv);
			/*arv_shutdown ();
			return EXIT_SUCCESS;*/

	}
	else if (g_strcmp0 (command, "procesado") == 0) {

		Mat image;
		/*image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file*/
		image = imread("/home/dmohedas/Escritorio/pingu.jpg", CV_LOAD_IMAGE_COLOR);


		    if(! image.data )                              // Check for invalid input
		    {
		        cout <<  "Could not open or find the image" << std::endl ;
		        cout <<  "No puedo mostrar la imagen" << std::endl ;
		        return -1;
		    }

		    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
		    imshow( "Display window", image );                   // Show our image inside it.
		    cout <<  "Debería mostrar la imagen" << std::endl ;
		    waitKey(0);                                          // Wait for a keystroke in the window

		    /*imwrite( "/home/dmohedas/Imágenes/Aravis/Gray_Image.jpg", image );*/

	} else if (g_strcmp0 (command, "valores") == 0) {
		arv_tool_list_features (genicam, "Root", ARV_TOOL_LIST_MODE_VALUES, 0);

	}else if (g_strcmp0 (command, "monocolor") == 0) {
		monocolorcambio(argc, argv);

	}

	else if (g_strcmp0 (command, "zoom") == 0) {
		zoomimagen(argc, argv);

	}

	else if (g_strcmp0 (command, "brillo") == 0) {
			brillo(argc, argv);

	}

	else if (g_strcmp0 (command, "videotest") == 0) {
			videostest();

		}

	else if (g_strcmp0 (command, "descripcion") == 0) {
		if (argc < 3)
			arv_tool_list_features (genicam, "Root", ARV_TOOL_LIST_MODE_DESCRIPTIONS, 0);
		else {
			int i;

			for (i = 2; i < argc; i++) {
				ArvGcNode *node;

				node = arv_gc_get_node (genicam, argv[i]);
				if (ARV_IS_GC_NODE (node)) {
					const char *description;

					printf ("%s: '%s'\n", arv_dom_node_get_node_name (ARV_DOM_NODE (node)), argv[i]);

					description = arv_gc_feature_node_get_description (ARV_GC_FEATURE_NODE (node));
					if (description)
						printf ("%s\n", description);
				}
			}
		}
	} else if (g_strcmp0 (command, "control") == 0) {
		int i;

		for (i = 2; i < argc; i++) {
			ArvGcNode *feature;
			char **tokens;

			tokens = g_strsplit (argv[i], "=", 2);
			feature = arv_device_get_feature (device, tokens[0]);
			if (ARV_IS_GC_FEATURE_NODE (feature)) {
				if (ARV_IS_GC_COMMAND (feature)) {
					arv_gc_command_execute (ARV_GC_COMMAND (feature), NULL);
					printf ("%s ejecutado\n", tokens[0]);
				} else {
					const char *unit;
					GError *error = NULL;

					if (tokens[1] != NULL)
						arv_gc_feature_node_set_value_from_string (ARV_GC_FEATURE_NODE (feature),
											   tokens[1], &error);

					if (error == NULL) {
						if (ARV_IS_GC_STRING (feature) ||
						    ARV_IS_GC_ENUMERATION (feature)) {
							const char *value = arv_gc_string_get_value (ARV_GC_STRING (feature), &error);

							if (error == NULL)
								printf ("%s = %s\n", tokens[0], value);
						} else if (ARV_IS_GC_INTEGER (feature)) {
							gint64 max_int64, min_int64, inc_int64;
							gint64 value;

							min_int64 = arv_gc_integer_get_min (ARV_GC_INTEGER (feature), NULL);
							max_int64 = arv_gc_integer_get_max (ARV_GC_INTEGER (feature), NULL);
							inc_int64 = arv_gc_integer_get_inc (ARV_GC_INTEGER (feature), NULL);
							unit = arv_gc_integer_get_unit (ARV_GC_INTEGER (feature), NULL);

							value = arv_gc_integer_get_value (ARV_GC_INTEGER (feature), &error);

							if (error == NULL) {
								GString *string = g_string_new ("");

								g_string_append_printf (string, "%s = %" G_GINT64_FORMAT, tokens[0], value);

								if (unit != NULL)
									g_string_append_printf (string, " %s", unit);
								if (min_int64 != G_MININT64)
									g_string_append_printf (string, " min:%" G_GINT64_FORMAT, min_int64);
								if (max_int64 != G_MAXINT64)
									g_string_append_printf (string, " max:%" G_GINT64_FORMAT, max_int64);
								if (inc_int64 != 1)
									g_string_append_printf (string, " inc:%" G_GINT64_FORMAT, inc_int64);

								printf ("%s\n", string->str);
								g_string_free (string, TRUE);
							}
						} else if (ARV_IS_GC_FLOAT (feature)) {
							double max_double, min_double, inc_double;
							GString *string = g_string_new ("");
							double value;

							min_double = arv_gc_float_get_min (ARV_GC_FLOAT (feature), NULL);
							max_double = arv_gc_float_get_max (ARV_GC_FLOAT (feature), NULL);
							inc_double = arv_gc_float_get_inc (ARV_GC_FLOAT (feature), NULL);
							unit = arv_gc_float_get_unit (ARV_GC_FLOAT (feature), NULL);

							value = arv_gc_float_get_value (ARV_GC_FLOAT (feature), &error);

							if (error == NULL) {
								g_string_append_printf (string, "%s = %g", tokens[0], value);

								if (unit != NULL)
									g_string_append_printf (string, " %s", unit);
								if (min_double != -G_MAXDOUBLE)
									g_string_append_printf (string, " min:%g", min_double);
								if (max_double != G_MAXDOUBLE)
									g_string_append_printf (string, " max:%g", max_double);
								if (inc_double != 1)
									g_string_append_printf (string, " inc:%g", inc_double);

								printf ("%s\n", string->str);
								g_string_free (string, TRUE);
							}
						} else if (ARV_IS_GC_BOOLEAN (feature)) {
							gboolean value = arv_gc_boolean_get_value (ARV_GC_BOOLEAN (feature), &error);

							if (error == NULL)
								printf ("%s = %s\n", tokens[0], value ?  "true" : "false");
						} else {
							const char *value =  arv_gc_feature_node_get_value_as_string
								(ARV_GC_FEATURE_NODE (feature), &error);

							if (error == NULL)
								printf ("%s = %s\n", tokens[0], value);
						}
					}

					if (error != NULL) {
							printf ("%s error de lectura: %s\n", tokens[0], error->message);
							g_clear_error (&error);
					}
				}
			} else {
				if (g_strrstr (tokens[0], "R[") == tokens[0]) {
					guint32 value;
					guint32 address;

					address = g_ascii_strtoll(&tokens[0][2], NULL, 0);

					if (tokens[1] != NULL) {
						arv_device_write_register (device,
									   address,
									   g_ascii_strtoll (tokens[1],
											    NULL, 0), NULL); /* TODO error handling */
					}

					arv_device_read_register (device, address, &value, NULL); /* TODO error handling */

					printf ("R[0x%08x] = 0x%08x\n",
						address, value);
				} else
					printf ("Parametro '%s' not found\n", tokens[0]);
			}
			g_strfreev (tokens);
		}
	} else {
		printf ("Comando desconocido\n");
	}

	if (arv_option_show_time)
		printf ("Ejecutado en %g s\n", (g_get_monotonic_time () - start) / 1000000.0);
}

int
main (int argc, char **argv)
{
	ArvDevice *device;
	ArvRegisterCachePolicy cache_policy = ARV_REGISTER_CACHE_POLICY_DEFAULT;
	const char *device_id;
	GOptionContext *context;
	GError *error = NULL;
	unsigned int n_devices;
	unsigned int i;

	context = g_option_context_new (" comando <parametrp>");
	g_option_context_set_summary (context, "Aplicacion para el control basico de un dispositivo Genicam.");
	g_option_context_set_description (context, description_content);
	g_option_context_add_main_entries (context, arv_option_entries, NULL);

	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_option_context_free (context);
		g_print ("Error al analizar la opción: %s\n", error->message);
		g_error_free (error);
		return EXIT_FAILURE;
	}

	g_option_context_free (context);

	if (arv_option_cache_policy == NULL ||
	    g_strcmp0 (arv_option_cache_policy, "disable") == 0)
		cache_policy = ARV_REGISTER_CACHE_POLICY_DISABLE;
	else if (g_strcmp0 (arv_option_cache_policy, "enable") == 0)
		cache_policy = ARV_REGISTER_CACHE_POLICY_ENABLE;
	else if (g_strcmp0 (arv_option_cache_policy, "debug") == 0)
		cache_policy = ARV_REGISTER_CACHE_POLICY_DEBUG;
	else {
		printf ("Cache no valido\n");
		return EXIT_FAILURE;
	}

	arv_debug_enable (arv_option_debug_domains);

	device_id = arv_option_device_address != NULL ? arv_option_device_address : arv_option_device_name;
	if (device_id != NULL) {
		GError *error = NULL;

		device = arv_open_device (device_id, &error);

		if (ARV_IS_DEVICE (device)) {
			if (argc < 2)
				printf ("%s\n", device_id);
			else
				arv_tool_execute_command (argc, argv, device, cache_policy);
			g_object_unref (device);
		} else {
			fprintf (stderr, "Dispositivo '%s' no encontrado%s%s\n", device_id,
				 error != NULL ? ": " : "",
				 error != NULL ? error->message : "");
			g_clear_error (&error);
		}
	} else {
		arv_update_device_list ();
		n_devices = arv_get_n_devices ();

		if (n_devices > 0) {
			for (i = 0; i < n_devices; i++) {
				GError *error = NULL;

				device_id = arv_get_device_id (i);
				device = arv_open_device (device_id, &error);

				if (ARV_IS_DEVICE (device)) {
					printf ("%s (%s)\n", device_id, arv_get_device_address (i));
					arv_tool_execute_command (argc, argv, device, cache_policy);

					g_object_unref (device);
				} else {
					fprintf (stderr, "Fallo al cargar dispositivo'%s'%s%s\n", device_id,
						 error != NULL ? ": " : "",
						 error != NULL ? error->message : "");
					g_clear_error (&error);
				}
			}
		} else {
			fprintf (stderr, "Dispositivo no encontrado\n");
		}
	}

	arv_shutdown ();

	return EXIT_SUCCESS;
}
